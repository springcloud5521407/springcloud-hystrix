package com.qytest.springcloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qytest.springcloud.dao.PaymentMapper;
import com.qytest.springcloud.entities.Payment;
import com.qytest.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author qy
 * @date 2022年07月8日 15:38
 */
@Service
public class PaymentServiceImpl extends ServiceImpl<PaymentMapper, Payment> implements PaymentService {

    @Override
    public String paymentInfoOk(Integer id) {
        int age = 10/0;
        return "线程池：" + Thread.currentThread().getName() + "成功访问paymentInfoOK,id=" + id;
    }

    @Override
    public String paymentInfoTimeOut(Integer id) {
        int time = 5;
       // int age = 10/0;
        try {
            TimeUnit.SECONDS.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "线程池：" + Thread.currentThread().getName() +  "耗时" + time + "秒成功访问paymentInfoTimeOut,id=" + id;
    }
}