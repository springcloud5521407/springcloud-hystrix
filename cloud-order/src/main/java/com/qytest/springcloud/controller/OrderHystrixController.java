package com.qytest.springcloud.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * http://localhost/order/hystrix/ok/1
 */
@RestController
@RequestMapping("/order")
@Slf4j
@DefaultProperties(defaultFallback="globalFallback")
public class OrderHystrixController {
    @Resource
    private PaymentHystrixService paymentHystrixService;

    @GetMapping(value = "/hystrix/ok/{id}")
    public String paymentInfoOk(@PathVariable("id") Integer id) {
        String result = paymentHystrixService.paymentInfoOk(id);
        log.info("========result:{}========", result);
        return result;
    }

   @HystrixCommand(fallbackMethod = "fallback",commandProperties = {
            @HystrixProperty(name ="execution.isolation.thread.timeoutInMilliseconds",value = "1500")
    })
    @GetMapping(value = "/hystrix/timeout/{id}")
    public String paymentInfoTimeOut(@PathVariable("id") Integer id) {
        String result = paymentHystrixService.paymentInfoTimeOut(id);
        log.info("========result:{}========", result);
        return result;
    }
 /*   public String fallback(Integer id){
        return "太久了我不想等待了，我已经等待了1.5s了！线程池：" + Thread.currentThread().getName() +"访问paymentInfoTimeOut,id=" + id;

    }*/

    //全局方法降级
    public String globalFallback(){
        return "全局降级！线程池：" + Thread.currentThread().getName() +"访问paymentInfoTimeOut";

    }
}
